#!/bin/bash
cd docs/"$1" || exit
for (( i = 0; i < 3; i++ )); do
    pdflatex -interaction=batchmode -output-format=pdf "$1".tex
done
find . -type f -not -name '*.tex'  -not -name '*.pdf' -delete