# Français
## Rappel du Projet
### Origine
Projet scolaire évaluant les unités de formation “Développement Web” et “Base de
Données”.

### Nature
Application Web développée en PHP 7.4, HTML 5, CSS 3, JavaScript.

### But et Intérêt
Évaluer les compétences des membres du projet.

## Rappel du planning
### Planning Général
*Dead-lines uniquement.*
1. **date inconnue** session de développement ;
2. **date inconnue** session de test ;
3. **date inconnue** présentation du projet au jury.

# English
## Project Recap
### Origine
School project evaluating formation units "Web Développement" and "Database".

### Nature
Web Application written in PHP 7.4, HTML 5, CSS 3, JavaScript.

### Goal
Evaluate the skills of the team's members.

## Planning Recap
### General Planning
*Dead-lines only.*
1. **unknown** coding session;
2. **unknown** testing session;
3. **unknown** confrontation with jury.